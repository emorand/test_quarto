---
title: Question sans réponse
---


Est-ce que les personnages masculins de games of thrones tuent plus que les femmes?

Réponse en 3 temps :
- qui tue?
- tue beaucoup?
- Et une représentation

L'objectif est d'aborder :

1. Définir la question , la réponse et l'outil
2. Quelques éléments de code pour travailler et retravailler des données
3. Re poser la question
4. Faire une representation