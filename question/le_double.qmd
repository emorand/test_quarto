---
title: C'est combien..
---
```{r importation2,include=FALSE}
perso <- read.csv("donnees/perso_final.csv", sep=",", stringsAsFactors=TRUE)
library(questionr)
```

Si il n'y pas de difference de "genre" dans le passage à l'acte (ici avoir tué au moins une fois), peut être que la répétition de l'action est différente.
L'analyse va donc ici se limiter aux "tueurs".


Pour réaliser cette analyse , on peut creer un tableau spécifique , dans lequel on aura que les tueurs


### Creer un sous-tableau

Il existe deux façons de faire ce code à l'ancienne

```{r sous_tab_v0}
# etablir la liste des lignes possedants la caractéristiques
liste<-which(perso$tueur=="oui")

# la selectionner
sous_tab<-perso[liste,]
sous_tab[1:5,]

```

```{r sous_tab_tidy}
library(dplyr)

sous_tab<-filter(perso, tueur=="oui")

sous_tab[1:5,]
```


et on effectue l'analyse prévue


### Faire l'analyse sur un sous-tableau 

```{r}
lprop(table(sous_tab$sexe,sous_tab$type_tueur))
 

```
